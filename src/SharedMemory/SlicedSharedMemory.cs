﻿using System;
using System.Threading;
using SharedMemory.Unofficial.System.IO.MemoryMappedFiles;

namespace SharedMemory {

	public class SlicedSharedMemory : IDisposable {
		private readonly EventWaitHandle _cyclicEvent;

		private readonly MemoryMappedFile _mappedMemory;
		private readonly MemoryMappedViewAccessor _readBlockAccessor;
		private readonly Thread _sharedMemoryThread;
		private readonly MemoryMappedViewAccessor _writeBlockAccessor;
		private bool _isClosed;

		public SlicedSharedMemory(string name, int readSize, int writeSize) {
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException(nameof(name), "Must not be null.");
			if (readSize < 0)
				throw new ArgumentOutOfRangeException(nameof(readSize), "Need non-negative number.");
			if (writeSize < 0)
				throw new ArgumentOutOfRangeException(nameof(writeSize), "Need non-negative number.");
			if (uint.MaxValue < (ulong) readSize + (ulong) writeSize)
				throw new ArgumentOutOfRangeException("readSize + writeSize",
					"Need to be smaller than max memory available.");
			MappedFileName = name;
			ReadSize = readSize;
			WriteSize = writeSize;

			_mappedMemory =
				MemoryMappedFile.CreateNew(MappedFileName, ReadSize + WriteSize, MemoryMappedFileAccess.ReadWrite);
			_readBlockAccessor = _mappedMemory.CreateViewAccessor(0, readSize, MemoryMappedFileAccess.Read);
			_writeBlockAccessor = _mappedMemory.CreateViewAccessor(0, readSize, MemoryMappedFileAccess.CopyOnWrite);

			_cyclicEvent = new EventWaitHandle(false, EventResetMode.AutoReset, MappedFileName + "_CyclicEvent");

			_sharedMemoryThread = new Thread(() => {
				while (_isClosed) {
					_cyclicEvent.WaitOne();
					Received?.Invoke(this, EventArgs.Empty);
					_cyclicEvent.Set();
				}
			});
			_sharedMemoryThread.Start();

			_isClosed = false;
		}

		public string MappedFileName { get; }
		public int ReadSize { get; }
		public int WriteSize { get; }

		public event EventHandler Received;

		public bool IsClosed() => _isClosed;

		public void Read<T>(long position, out T result) where T : struct {
			if (position > ReadSize)
				throw new ArgumentOutOfRangeException(nameof(position), position,
					"Value is out of reading memory space boundaries.");
			_readBlockAccessor.Read(position, out result);
		}

		public void ReadArray<T>(long position, T[] array, int offset, int count) where T : struct {
			if (position > ReadSize)
				throw new ArgumentOutOfRangeException(nameof(position), position,
					"Value is out of reading memory space boundaries.");
			_readBlockAccessor.ReadArray(position, array, offset, count);
		}

		public void Write<T>(long position, ref T value) where T : struct {
			if (position > WriteSize)
				throw new ArgumentOutOfRangeException(nameof(position), position,
					"Value is out of writing memory space boundaries.");
			_writeBlockAccessor.Write(ReadSize + position, ref value);
		}

		public void WriteArray<T>(long position, T[] array, int offset, int count) where T : struct {
			if (position > WriteSize)
				throw new ArgumentOutOfRangeException(nameof(position), position,
					"Value is out of writing memory space boundaries.");
			_writeBlockAccessor.WriteArray(ReadSize + position, array, offset, count);
		}

		public void Close() => Dispose(true);

		#region Disposable Pattern

		~SlicedSharedMemory() => Dispose(false);

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing) {
			if (disposing && !_isClosed) {
				_sharedMemoryThread.Join(100);
				_mappedMemory?.Dispose();
				_readBlockAccessor?.Dispose();
				_writeBlockAccessor?.Dispose();
				((IDisposable) _cyclicEvent)?.Dispose();
			}

			_isClosed = true;
		}

		#endregion

	}
}