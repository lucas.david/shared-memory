#if !NET40Plus

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Security;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Threading;
using Microsoft.Win32.SafeHandles;
using SharedMemory.Unofficial.Microsoft.Win32;
using SharedMemory.Unofficial.Microsoft.Win32.SafeHandles;

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {

	public class MemoryMappedFile : IDisposable {

		#region Members

		private readonly FileStream _fileStream;
		private readonly bool _leaveOpen;
		internal const int DefaultSize = 0;

		#endregion Members

		#region Constructors

		[SecurityCritical]
		private MemoryMappedFile(SafeMemoryMappedFileHandle handle) {
			Debug.Assert(handle != null && !handle.IsClosed && !handle.IsInvalid, "handle is null, closed, or invalid");

			SafeMemoryMappedFileHandle = handle;
			_leaveOpen = true;
		}

		[SecurityCritical]
		private MemoryMappedFile(SafeMemoryMappedFileHandle handle, FileStream fileStream, bool leaveOpen) {
			Debug.Assert(handle != null && !handle.IsClosed && !handle.IsInvalid, "handle is null, closed, or invalid");
			Debug.Assert(fileStream != null, "fileStream is null");

			SafeMemoryMappedFileHandle = handle;
			_fileStream = fileStream;
			_leaveOpen = leaveOpen;
		}

		#endregion Constructors

		#region Public Static Methods

		public static MemoryMappedFile OpenExisting(string mapName) =>
			OpenExisting(mapName, MemoryMappedFileRights.ReadWrite, HandleInheritability.None);

		public static MemoryMappedFile OpenExisting(string mapName, MemoryMappedFileRights desiredAccessRights) =>
			OpenExisting(mapName, desiredAccessRights, HandleInheritability.None);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static MemoryMappedFile OpenExisting(string mapName, MemoryMappedFileRights desiredAccessRights,
			HandleInheritability inheritability) {
			if (string.IsNullOrEmpty(mapName))
				throw new ArgumentException("cannot be null or empty.", nameof(mapName));
			if (inheritability < HandleInheritability.None || inheritability > HandleInheritability.Inheritable)
				throw new ArgumentOutOfRangeException(nameof(inheritability));
			if (((int) desiredAccessRights &
				~(int) (MemoryMappedFileRights.FullControl | MemoryMappedFileRights.AccessSystemSecurity)) != 0)
				throw new ArgumentOutOfRangeException(nameof(desiredAccessRights));

			SafeMemoryMappedFileHandle handle = OpenCore(mapName, inheritability, (int) desiredAccessRights, false);
			return new MemoryMappedFile(handle);
		}

		public static MemoryMappedFile CreateFromFile(string path) =>
			CreateFromFile(path, FileMode.Open, null, DefaultSize, MemoryMappedFileAccess.ReadWrite);

		public static MemoryMappedFile CreateFromFile(string path, FileMode mode) =>
			CreateFromFile(path, mode, null, DefaultSize, MemoryMappedFileAccess.ReadWrite);

		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName) =>
			CreateFromFile(path, mode, mapName, DefaultSize, MemoryMappedFileAccess.ReadWrite);

		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName, long capacity) =>
			CreateFromFile(path, mode, mapName, capacity, MemoryMappedFileAccess.ReadWrite);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static MemoryMappedFile CreateFromFile(string path, FileMode mode, string mapName, long capacity,
			MemoryMappedFileAccess access) {
			if (path == null)
				throw new ArgumentNullException(nameof(path), "cannot be null.");
			if (string.IsNullOrEmpty(mapName))
				throw new ArgumentException("cannot be null or empty", nameof(mapName));
			if (capacity < 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), capacity,
					" must be larger than or equal to 0.");
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (mode == FileMode.Append)
				throw new ArgumentException("cannot be FileMode.Append while creating from file.", nameof(mode));
			if (access == MemoryMappedFileAccess.Write)
				throw new ArgumentException("cannot be MemoryMappedFilAccess.Write while creating from file.",
					nameof(access));

			bool existed = File.Exists(path);
			var fileStream = new FileStream(path, mode, GetFileStreamFileSystemRights(access), FileShare.None, 0x1000,
				FileOptions.None);

			if (capacity == 0 && fileStream.Length == 0) {
				CleanupFile(fileStream, existed, path);
				throw new ArgumentException();
			}

			if (access == MemoryMappedFileAccess.Read && capacity > fileStream.Length) {
				CleanupFile(fileStream, existed, path);
				throw new ArgumentException();
			}

			if (capacity == DefaultSize)
				capacity = fileStream.Length;

			if (fileStream.Length > capacity) {
				CleanupFile(fileStream, existed, path);
				throw new ArgumentOutOfRangeException(nameof(capacity));
			}

			SafeMemoryMappedFileHandle handle;
			try {
				handle = CreateCore(fileStream.SafeFileHandle, mapName, HandleInheritability.None, null, access,
					MemoryMappedFileOptions.None, capacity);
			} catch {
				CleanupFile(fileStream, existed, path);
				throw;
			}

			Debug.Assert(handle != null && !handle.IsInvalid);
			return new MemoryMappedFile(handle, fileStream, false);
		}

		public static MemoryMappedFile CreateFromFile(FileStream fileStream, string mapName, long capacity,
			MemoryMappedFileAccess access, HandleInheritability inheritability, bool leaveOpen) =>
			CreateFromFile(fileStream, mapName, capacity, access, null, inheritability, leaveOpen);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static MemoryMappedFile CreateFromFile(FileStream fileStream, string mapName, long capacity,
			MemoryMappedFileAccess access, MemoryMappedFileSecurity memoryMappedFileSecurity,
			HandleInheritability inheritability, bool leaveOpen) {
			if (fileStream == null)
				throw new ArgumentNullException(nameof(fileStream), "cannot be null.");
			if (string.IsNullOrEmpty(mapName))
				throw new ArgumentException("cannot be null or empty.", nameof(mapName));
			if (capacity < 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "must be larger than or equal to 0.");
			if (capacity == 0 && fileStream.Length == 0)
				throw new ArgumentException();
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (access == MemoryMappedFileAccess.Write)
				throw new ArgumentException("cannot be MemoryMappedFileAccess.Write while creating from file.",
					nameof(access));
			if (access == MemoryMappedFileAccess.Read && capacity > fileStream.Length)
				throw new ArgumentException();
			if (inheritability < HandleInheritability.None || inheritability > HandleInheritability.Inheritable)
				throw new ArgumentOutOfRangeException(nameof(inheritability));
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			fileStream.Flush();

			if (capacity == DefaultSize)
				capacity = fileStream.Length;

			if (fileStream.Length > capacity)
				throw new ArgumentOutOfRangeException(nameof(capacity));

			SafeMemoryMappedFileHandle handle = CreateCore(fileStream.SafeFileHandle, mapName, inheritability,
				memoryMappedFileSecurity, access, MemoryMappedFileOptions.None, capacity);

			return new MemoryMappedFile(handle, fileStream, leaveOpen);
		}

		public static MemoryMappedFile CreateNew(string mapName, long capacity) =>
			CreateNew(mapName, capacity, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, null,
				HandleInheritability.None);

		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access) =>
			CreateNew(mapName, capacity, access, MemoryMappedFileOptions.None, null, HandleInheritability.None);

		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access,
			MemoryMappedFileOptions options, HandleInheritability inheritability) =>
			CreateNew(mapName, capacity, access, options, null, inheritability);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static MemoryMappedFile CreateNew(string mapName, long capacity, MemoryMappedFileAccess access,
			MemoryMappedFileOptions options, MemoryMappedFileSecurity memoryMappedFileSecurity,
			HandleInheritability inheritability) {
			if (string.IsNullOrEmpty(mapName))
				throw new ArgumentException("cannot be null or empty.", nameof(mapName));
			if (capacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "must be larger than 0.");
			if (IntPtr.Size == 4 && capacity > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(capacity));
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (access == MemoryMappedFileAccess.Write)
				throw new ArgumentException("cannot be MemoryMappedFileAccess.Write while creating new.",
					nameof(access));
			if (((int) options & ~(int) MemoryMappedFileOptions.DelayAllocatePages) != 0)
				throw new ArgumentOutOfRangeException(nameof(options));
			if (inheritability < HandleInheritability.None || inheritability > HandleInheritability.Inheritable)
				throw new ArgumentOutOfRangeException(nameof(inheritability));
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			SafeMemoryMappedFileHandle handle = CreateCore(new SafeFileHandle(new IntPtr(-1), true), mapName,
				inheritability, memoryMappedFileSecurity, access, options, capacity);
			return new MemoryMappedFile(handle);
		}

		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity) =>
			CreateOrOpen(mapName, capacity, MemoryMappedFileAccess.ReadWrite, MemoryMappedFileOptions.None, null,
				HandleInheritability.None);

		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access) =>
			CreateOrOpen(mapName, capacity, access, MemoryMappedFileOptions.None, null, HandleInheritability.None);

		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access,
			MemoryMappedFileOptions options, HandleInheritability inheritability) =>
			CreateOrOpen(mapName, capacity, access, options, null, inheritability);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public static MemoryMappedFile CreateOrOpen(string mapName, long capacity, MemoryMappedFileAccess access,
			MemoryMappedFileOptions options, MemoryMappedFileSecurity memoryMappedFileSecurity,
			HandleInheritability inheritability) {
			if (string.IsNullOrEmpty(mapName))
				throw new ArgumentException("cannot be null or empty.", nameof(mapName));
			if (capacity <= 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "must be larger than 0.");
			if (IntPtr.Size == 4 && capacity > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(capacity));
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (((int) options & ~(int) MemoryMappedFileOptions.DelayAllocatePages) != 0)
				throw new ArgumentOutOfRangeException(nameof(options));
			if (inheritability < HandleInheritability.None || inheritability > HandleInheritability.Inheritable)
				throw new ArgumentOutOfRangeException(nameof(inheritability));
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			SafeMemoryMappedFileHandle handle;
			// Special case for write access; create will never succeed.
			if (access == MemoryMappedFileAccess.Write)
				handle = OpenCore(mapName, inheritability, GetFileMapAccess(access), true);
			else
				handle = CreateOrOpenCore(new SafeFileHandle(new IntPtr(-1), true), mapName, inheritability,
					memoryMappedFileSecurity, access, options, capacity);

			return new MemoryMappedFile(handle);
		}

		#endregion Public Static Methods

		#region Private Static Methods

		[SecurityCritical]
		private static SafeMemoryMappedFileHandle CreateCore(SafeFileHandle fileHandle, string mapName,
			HandleInheritability inheritability, MemoryMappedFileSecurity memoryMappedFileSecurity,
			MemoryMappedFileAccess access, MemoryMappedFileOptions options, long capacity) {
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			SafeMemoryMappedFileHandle handle;
			UnsafeNativeMethods.SECURITY_ATTRIBUTES secAttrs =
				GetSecAttrs(inheritability, memoryMappedFileSecurity, out object pinningHandle);

			var capacityLow = (int) (capacity & 0x00000000FFFFFFFFL);
			var capacityHigh = (int) (capacity >> 32);

			try {
				handle = UnsafeNativeMethods.CreateFileMapping(fileHandle, secAttrs,
					GetPageAccess(access) | (int) options, capacityHigh, capacityLow, mapName);

				int errorCode = Marshal.GetLastWin32Error();
				if (!handle.IsInvalid && errorCode == UnsafeNativeMethods.ERROR_ALREADY_EXISTS) {
					handle.Dispose();
					throw new IOException(UnsafeNativeMethods.GetMessage(errorCode));
				} else if (handle.IsInvalid)
					throw new IOException(UnsafeNativeMethods.GetMessage(errorCode));
			} finally {
				if (pinningHandle != null) {
					var pinHandle = (GCHandle) pinningHandle;
					pinHandle.Free();
				}
			}

			return handle;
		}

		[SecurityCritical]
		private static SafeMemoryMappedFileHandle OpenCore(string mapName, HandleInheritability inheritability,
			int desiredAccessRights, bool createOrOpen) {
			SafeMemoryMappedFileHandle handle = UnsafeNativeMethods.OpenFileMapping(desiredAccessRights,
				(inheritability & HandleInheritability.Inheritable) != 0, mapName);
			int lastError = Marshal.GetLastWin32Error();

			if (handle.IsInvalid) {
				if (createOrOpen && lastError == UnsafeNativeMethods.ERROR_FILE_NOT_FOUND)
					throw new ArgumentException("", nameof(mapName));
				throw new IOException(UnsafeNativeMethods.GetMessage(lastError));
			}

			return handle;
		}

		[SecurityCritical]
		private static SafeMemoryMappedFileHandle CreateOrOpenCore(SafeFileHandle fileHandle, string mapName,
			HandleInheritability inheritability, MemoryMappedFileSecurity memoryMappedFileSecurity,
			MemoryMappedFileAccess access, MemoryMappedFileOptions options, long capacity) {
			Debug.Assert(access != MemoryMappedFileAccess.Write,
				"Callers requesting write access shouldn't try to create a mmf");

			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			SafeMemoryMappedFileHandle handle = null;
			UnsafeNativeMethods.SECURITY_ATTRIBUTES secAttrs =
				GetSecAttrs(inheritability, memoryMappedFileSecurity, out object pinningHandle);

			var capacityLow = (int) (capacity & 0x00000000FFFFFFFFL);
			var capacityHigh = (int) (capacity >> 32);

			try {
				var waitRetries = 14; // ((2^13) * -1) * 10ms == approximately 1.4 mins
				var waitSleep = 0;

				while (waitRetries > 0) {
					// Try to create.
					handle = UnsafeNativeMethods.CreateFileMapping(fileHandle, secAttrs,
						GetPageAccess(access) | (int) options, capacityHigh, capacityLow, mapName);

					int createErrorCode = Marshal.GetLastWin32Error();
					if (!handle.IsInvalid)
						break;
					if (createErrorCode != UnsafeNativeMethods.ERROR_ACCESS_DENIED)
						throw new IOException(UnsafeNativeMethods.GetMessage(createErrorCode));

					handle.SetHandleAsInvalid();

					handle = UnsafeNativeMethods.OpenFileMapping(GetFileMapAccess(access),
						(inheritability & HandleInheritability.Inheritable) != 0, mapName);

					int openErrorCode = Marshal.GetLastWin32Error();

					if (!handle.IsInvalid)
						break;
					if (openErrorCode != UnsafeNativeMethods.ERROR_FILE_NOT_FOUND)
						throw new IOException(UnsafeNativeMethods.GetMessage(openErrorCode));

					--waitRetries;
					if (waitSleep == 0)
						waitSleep = 10;
					else {
						Thread.Sleep(waitSleep);
						waitSleep *= 2;
					}
				}

				if (handle == null || handle.IsInvalid)
					throw new InvalidOperationException();
			} finally {
				if (pinningHandle != null) {
					var pinHandle = (GCHandle) pinningHandle;
					pinHandle.Free();
				}
			}

			return handle;
		}

		#endregion Private Static Methods

		#region Public Methods

		public MemoryMappedViewStream CreateViewStream() =>
			CreateViewStream(0, DefaultSize, MemoryMappedFileAccess.ReadWrite);

		public MemoryMappedViewStream CreateViewStream(long offset, long size) =>
			CreateViewStream(offset, size, MemoryMappedFileAccess.ReadWrite);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public MemoryMappedViewStream CreateViewStream(long offset, long size, MemoryMappedFileAccess access) {
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset));
			if (size < 0)
				throw new ArgumentOutOfRangeException(nameof(size));
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (IntPtr.Size == 4 && size > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(size));

			MemoryMappedView view = MemoryMappedView.CreateView(SafeMemoryMappedFileHandle, access, offset, size);
			return new MemoryMappedViewStream(view);
		}

		// Creates a new view in the form of an accessor. Accessors are for random access.
		public MemoryMappedViewAccessor CreateViewAccessor() =>
			CreateViewAccessor(0, DefaultSize, MemoryMappedFileAccess.ReadWrite);

		public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size) =>
			CreateViewAccessor(offset, size, MemoryMappedFileAccess.ReadWrite);

		[SecurityCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public MemoryMappedViewAccessor CreateViewAccessor(long offset, long size, MemoryMappedFileAccess access) {
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "must be larger than or equal to 0.");
			if (size < 0)
				throw new ArgumentOutOfRangeException(nameof(size), "must be larger than or equal to 0.");
			if (access < MemoryMappedFileAccess.ReadWrite || access > MemoryMappedFileAccess.ReadWriteExecute)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (IntPtr.Size == 4 && size > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(size),
					string.Format("must be smaller than {0}", uint.MaxValue));

			MemoryMappedView view = MemoryMappedView.CreateView(SafeMemoryMappedFileHandle, access, offset, size);
			return new MemoryMappedViewAccessor(view);
		}

		#endregion Public Methods

		#region Disposable Pattern

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		[SecuritySafeCritical]
		protected virtual void Dispose(bool disposing) {
			try {
				if (SafeMemoryMappedFileHandle != null && !SafeMemoryMappedFileHandle.IsClosed)
					SafeMemoryMappedFileHandle.Dispose();
			} finally {
				if (_fileStream != null && _leaveOpen == false)
					_fileStream.Dispose();
			}
		}

		#endregion Disposable Pattern

		#region Public Accessors

		public SafeMemoryMappedFileHandle SafeMemoryMappedFileHandle {
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get;
		}

		[SecurityCritical]
		public MemoryMappedFileSecurity GetAccessControl() =>
			throw new NotSupportedException(
				"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

		[ResourceExposure(ResourceScope.Machine)]
		[ResourceConsumption(ResourceScope.Machine)]
		[SecurityCritical]
		public void SetAccessControl(MemoryMappedFileSecurity memoryMappedFileSecurity) {
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException("Non-null MemoryMappedFileSecurity argument not yet supported.");

			if (memoryMappedFileSecurity == null)
				throw new ArgumentNullException(nameof(memoryMappedFileSecurity));
			/*
			if (_handle.IsClosed)
				__Error.FileNotOpen();

			memoryMappedFileSecurity.PersistHandle(_handle);
			*/
		}

		#endregion Public Accessors

		#region Internal/Private Static Methods

		[SecurityCritical]
		internal static int GetSystemPageAllocationGranularity() {
			var info = new UnsafeNativeMethods.SYSTEM_INFO();
			UnsafeNativeMethods.GetSystemInfo(ref info);

			return info.dwAllocationGranularity;
		}

		internal static int GetPageAccess(MemoryMappedFileAccess access) {
			switch (access) {
				case MemoryMappedFileAccess.Read:
					return UnsafeNativeMethods.PAGE_READONLY;
				case MemoryMappedFileAccess.ReadWrite:
					return UnsafeNativeMethods.PAGE_READWRITE;
				case MemoryMappedFileAccess.CopyOnWrite:
					return UnsafeNativeMethods.PAGE_WRITECOPY;
				case MemoryMappedFileAccess.ReadExecute:
					return UnsafeNativeMethods.PAGE_EXECUTE_READ;
				case MemoryMappedFileAccess.ReadWriteExecute:
					return UnsafeNativeMethods.PAGE_EXECUTE_READWRITE;
				default:
					throw new ArgumentOutOfRangeException(nameof(access), "is invalid.");
			}
		}

		internal static int GetFileMapAccess(MemoryMappedFileAccess access) {
			switch (access) {
				case MemoryMappedFileAccess.Read:
					return UnsafeNativeMethods.FILE_MAP_READ;
				case MemoryMappedFileAccess.Write:
					return UnsafeNativeMethods.FILE_MAP_WRITE;
				case MemoryMappedFileAccess.ReadWrite:
					return UnsafeNativeMethods.FILE_MAP_READ | UnsafeNativeMethods.FILE_MAP_WRITE;
				case MemoryMappedFileAccess.CopyOnWrite:
					return UnsafeNativeMethods.FILE_MAP_COPY;
				case MemoryMappedFileAccess.ReadExecute:
					return UnsafeNativeMethods.FILE_MAP_EXECUTE | UnsafeNativeMethods.FILE_MAP_READ;
				case MemoryMappedFileAccess.ReadWriteExecute:
					return UnsafeNativeMethods.FILE_MAP_EXECUTE | UnsafeNativeMethods.FILE_MAP_READ |
						UnsafeNativeMethods.FILE_MAP_WRITE;
				default:
					throw new ArgumentOutOfRangeException(nameof(access), "is invalid.");
			}
		}

		private static FileSystemRights GetFileStreamFileSystemRights(MemoryMappedFileAccess access) {
			switch (access) {
				case MemoryMappedFileAccess.Read:
				case MemoryMappedFileAccess.CopyOnWrite:
					return FileSystemRights.ReadData;
				case MemoryMappedFileAccess.ReadWrite:
					return FileSystemRights.ReadData | FileSystemRights.WriteData;
				case MemoryMappedFileAccess.Write:
					return FileSystemRights.WriteData;
				case MemoryMappedFileAccess.ReadExecute:
					return FileSystemRights.ReadData | FileSystemRights.ExecuteFile;
				case MemoryMappedFileAccess.ReadWriteExecute:
					return FileSystemRights.ReadData | FileSystemRights.WriteData | FileSystemRights.ExecuteFile;
				default:
					throw new ArgumentOutOfRangeException(nameof(access), "is invalid.");
			}
		}

		internal static FileAccess GetFileAccess(MemoryMappedFileAccess access) {
			switch (access) {
				case MemoryMappedFileAccess.Read:
					return FileAccess.Read;
				case MemoryMappedFileAccess.Write:
					return FileAccess.Write;
				case MemoryMappedFileAccess.ReadWrite:
				case MemoryMappedFileAccess.CopyOnWrite:
					return FileAccess.ReadWrite;
				case MemoryMappedFileAccess.ReadExecute:
					return FileAccess.Read;
				case MemoryMappedFileAccess.ReadWriteExecute:
					return FileAccess.ReadWrite;
				default:
					throw new ArgumentOutOfRangeException(nameof(access), "is invalid.");
			}
		}

		[SecurityCritical]
		private static UnsafeNativeMethods.SECURITY_ATTRIBUTES GetSecAttrs(HandleInheritability inheritability,
			MemoryMappedFileSecurity memoryMappedFileSecurity, out object pinningHandle) {
			if (memoryMappedFileSecurity != null)
				throw new NotSupportedException(
					"Non-null MemoryMappedFileSecurity argument not supported before .NET Framework 4.0");

			pinningHandle = null;
			UnsafeNativeMethods.SECURITY_ATTRIBUTES secAttrs = null;
			if ((inheritability & HandleInheritability.Inheritable) != 0) {
				secAttrs = new UnsafeNativeMethods.SECURITY_ATTRIBUTES();
				secAttrs.nLength = Marshal.SizeOf(secAttrs);

				if ((inheritability & HandleInheritability.Inheritable) != 0)
					secAttrs.bInheritHandle = 1;
			}

			return secAttrs;
		}

		private static void CleanupFile(FileStream fileStream, bool existed, string path) {
			fileStream.Close();
			if (!existed)
				File.Delete(path);
		}
	}

	#endregion Internal/Private Static Methods

}

#endif