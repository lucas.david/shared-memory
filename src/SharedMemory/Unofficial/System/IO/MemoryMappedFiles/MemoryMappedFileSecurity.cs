#if !NET40Plus

using System;

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {
	[Flags]
	public enum MemoryMappedFileRights {
		/* These correspond to win32 FILE_MAP_XXX constants. */

		CopyOnWrite = 0x000001,
		Write = 0x000002,
		Read = 0x000004,
		Execute = 0x000008,

		Delete = 0x010000,
		ReadPermissions = 0x020000,
		ChangePermissions = 0x040000,
		TakeOwnership = 0x080000,
		/* Synchronize     = Not supported by memory mapped files. */

		ReadWrite = Read | Write,
		ReadExecute = Read | Execute,
		ReadWriteExecute = Read | Write | Execute,

		FullControl = CopyOnWrite | Read | Write | Execute | Delete | ReadPermissions | ChangePermissions |
			TakeOwnership,

		AccessSystemSecurity = 0x01000000
	}

	/* Defined for compatibility. */
	public class MemoryMappedFileSecurity { }
}

#endif