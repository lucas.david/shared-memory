﻿#if !NET40Plus

using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

namespace SharedMemory.Unofficial.System.Runtime.InteropServices {
	public static class Marshal {

		#region Public Static Methods 'SizeOf'

		/**
		 * <summary>
		 *	Retourne la taille non managée d'un objet en octets.
		 * </summary>
		 * <param name="structure">
		 *	Objet dont la taille doit être retournée.
		 * </param>
		 * <returns>Taille de l'objet spécifié dans le code non managé.</returns>
		 * <exception cref="T:System.ArgumentNullException">
		 *	Le paramètre <paramref name="structure" /> a la valeur <see langword="null" />.
		 * </exception>
		 */
		[ResourceExposure(ResourceScope.None)]
		[ComVisible(true)]
		public static int SizeOf(object structure) {
			if (structure == null)
				throw new ArgumentNullException(nameof(structure));
			return SizeOf(structure.GetType());
		}

		public static int SizeOf(Type t) => global::System.Runtime.InteropServices.Marshal.SizeOf(t);

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		internal static uint SizeOfType(Type type) => (uint) SizeOf(type);

		public static int SizeOf<T>() => SizeOf(typeof(T));

		public static int SizeOf<T>(T structure) => SizeOf((object) structure);

		/**
		 * <summary>
		 *	Returns the aligned size of an instance of a value type.
		 * </summary>
		 * <typeparam name="T">Provide a value type to figure out its size</typeparam>
		 * <returns>The aligned size of T in bytes.</returns>
		 */
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		internal static uint AlignedSizeOf<T>() where T : struct {
			uint size = SizeOfType(typeof(T));
			if (size == 1 || size == 2)
				return size;
			if (IntPtr.Size == 8 && size == 4)
				return size;
			return size + 3 & ~(uint) 3;
		}

		#endregion Public Static Methods 'SizeOf'

	}
}

#endif