﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Shared Memory")]
[assembly:
	AssemblyDescription(
		"The Shared Memory class library provides an unofficial support for memory-mapped files on C#/.NET Framework 3.5")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("SharedMemory")]
[assembly: AssemblyCopyright("Copyright © Lucas David 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("88856320-02b0-431d-b1f2-37d2d5252fcf")]
[assembly: AssemblyVersion("0.0.2.*")]
[assembly: NeutralResourcesLanguage("")]