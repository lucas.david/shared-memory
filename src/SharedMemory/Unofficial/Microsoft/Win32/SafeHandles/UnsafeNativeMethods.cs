#if !NET40Plus

using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace SharedMemory.Unofficial.Microsoft.Win32.SafeHandles {

	[SuppressUnmanagedCodeSecurity]
	internal static class UnsafeNativeMethods {

		internal const string KERNEL32 = "kernel32.dll";

		/* Win32 IO */
		internal const int CREDUI_MAX_USERNAME_LENGTH = 513;

		// for win32 error message formatting
		private const int FORMAT_MESSAGE_IGNORE_INSERTS = 0x00000200;
		private const int FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000;
		private const int FORMAT_MESSAGE_ARGUMENT_ARRAY = 0x00002000;
		internal static readonly IntPtr NULL = IntPtr.Zero;


		[DllImport(KERNEL32, CharSet = CharSet.Auto, BestFitMapping = false)]
		[SecurityCritical]
		internal static extern int FormatMessage(int dwFlags, IntPtr lpSource, int dwMessageId, int dwLanguageId,
			StringBuilder lpBuffer, int nSize, IntPtr va_list_arguments);

		[SecurityCritical]
		internal static string GetMessage(int errorCode) {
			var sb = new StringBuilder(512);
			int result = FormatMessage(
				FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY, NULL,
				errorCode, 0, sb, sb.Capacity, NULL);
			if (result != 0)
				return sb.ToString();
			return "UnknownError_Num " + errorCode;
		}

		#region Pipe

		[DllImport(KERNEL32, SetLastError = true)]
		[return: MarshalAs(UnmanagedType.Bool)]
		[SecurityCritical]
		internal static extern bool CloseHandle(IntPtr handle);

		#endregion

		[StructLayout(LayoutKind.Sequential)]
		internal class SECURITY_ATTRIBUTES {
			internal int bInheritHandle;
			internal int nLength;
			[SecurityCritical] internal unsafe byte* pSecurityDescriptor;
		}

		#region WinError

		internal const int ERROR_SUCCESS = 0x0;
		internal const int ERROR_FILE_NOT_FOUND = 0x2;
		internal const int ERROR_PATH_NOT_FOUND = 0x3;
		internal const int ERROR_ACCESS_DENIED = 0x5;
		internal const int ERROR_INVALID_HANDLE = 0x6;

		internal const int ERROR_NOT_ENOUGH_MEMORY = 0x8;

		internal const int ERROR_INVALID_DRIVE = 0xF;
		internal const int ERROR_NO_MORE_FILES = 0x12;
		internal const int ERROR_NOT_READY = 0x15;
		internal const int ERROR_BAD_LENGTH = 0x18;
		internal const int ERROR_SHARING_VIOLATION = 0x20;
		internal const int ERROR_LOCK_VIOLATION = 0x21;
		internal const int ERROR_HANDLE_EOF = 0x26;
		internal const int ERROR_FILE_EXISTS = 0x50;
		internal const int ERROR_INVALID_PARAMETER = 0x57;
		internal const int ERROR_BROKEN_PIPE = 0x6D;
		internal const int ERROR_INSUFFICIENT_BUFFER = 0x7A;
		internal const int ERROR_INVALID_NAME = 0x7B;
		internal const int ERROR_BAD_PATHNAME = 0xA1;
		internal const int ERROR_ALREADY_EXISTS = 0xB7;
		internal const int ERROR_ENVVAR_NOT_FOUND = 0xCB;
		internal const int ERROR_FILENAME_EXCED_RANGE = 0xCE;
		internal const int ERROR_PIPE_BUSY = 0xE7;
		internal const int ERROR_NO_DATA = 0xE8;
		internal const int ERROR_PIPE_NOT_CONNECTED = 0xE9;
		internal const int ERROR_MORE_DATA = 0xEA;
		internal const int ERROR_NO_MORE_ITEMS = 0x103;
		internal const int ERROR_PIPE_CONNECTED = 0x217;
		internal const int ERROR_PIPE_LISTENING = 0x218;
		internal const int ERROR_OPERATION_ABORTED = 0x3E3;
		internal const int ERROR_IO_PENDING = 0x3E5;
		internal const int ERROR_NOT_FOUND = 0x490;

		#endregion

		#region Memory Mapped Files Constants

		internal const int MEM_COMMIT = 0x1000;
		internal const int MEM_RESERVE = 0x2000;
		internal const int INVALID_FILE_SIZE = -1;
		internal const int PAGE_READWRITE = 0x04;
		internal const int PAGE_READONLY = 0x02;
		internal const int PAGE_WRITECOPY = 0x08;
		internal const int PAGE_EXECUTE_READ = 0x20;
		internal const int PAGE_EXECUTE_READWRITE = 0x40;

		internal const int FILE_MAP_COPY = 0x0001;
		internal const int FILE_MAP_WRITE = 0x0002;
		internal const int FILE_MAP_READ = 0x0004;
		internal const int FILE_MAP_EXECUTE = 0x0020;

		#endregion

		#region Memory Mapped File

		[StructLayout(LayoutKind.Sequential)]
		
		#pragma warning restore 618
		internal unsafe struct MEMORY_BASIC_INFORMATION {
			internal void* BaseAddress;
			internal void* AllocationBase;
			internal uint AllocationProtect;
			internal UIntPtr RegionSize;
			internal uint State;
			internal uint Protect;
			internal uint Type;
		}
		#pragma warning disable 618 // System.Core still uses SecurityRuleSet.Level1
		[SecurityCritical(SecurityCriticalScope.Everything)]

		[StructLayout(LayoutKind.Sequential)]
		internal struct SYSTEM_INFO {
			internal int dwOemId; // This is a union of a DWORD and a struct containing 2 WORDs.
			internal int dwPageSize;
			internal IntPtr lpMinimumApplicationAddress;
			internal IntPtr lpMaximumApplicationAddress;
			internal IntPtr dwActiveProcessorMask;
			internal int dwNumberOfProcessors;
			internal int dwProcessorType;
			internal int dwAllocationGranularity;
			internal short wProcessorLevel;
			internal short wProcessorRevision;
		}

		[DllImport(KERNEL32, SetLastError = true)]
		[SecurityCritical]
		internal static extern void GetSystemInfo(ref SYSTEM_INFO lpSystemInfo);

		[DllImport(KERNEL32, ExactSpelling = true)]
		[SecurityCritical]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

		[DllImport(KERNEL32, SetLastError = true)]
		[SecurityCritical]
		internal static extern int GetFileSize(SafeMemoryMappedFileHandle hFile, out int highSize);

		[DllImport(KERNEL32, SetLastError = true)]
		[SecurityCritical]
		internal static extern IntPtr VirtualQuery(SafeMemoryMappedViewHandle address,
			ref MEMORY_BASIC_INFORMATION buffer, IntPtr sizeOfBuffer);

		[DllImport(KERNEL32, SetLastError = true, CharSet = CharSet.Auto, BestFitMapping = false)]
		[SecurityCritical]
		internal static extern SafeMemoryMappedFileHandle CreateFileMapping(SafeFileHandle hFile,
			SECURITY_ATTRIBUTES lpAttributes, int fProtect, int dwMaximumSizeHigh, int dwMaximumSizeLow, string lpName);

		[DllImport(KERNEL32, ExactSpelling = true, SetLastError = true)]
		[SecurityCritical]
		[return: MarshalAs(UnmanagedType.Bool)]
		internal static extern unsafe bool FlushViewOfFile(byte* lpBaseAddress, IntPtr dwNumberOfBytesToFlush);

		[DllImport(KERNEL32, SetLastError = true, CharSet = CharSet.Auto, BestFitMapping = false)]
		[SecurityCritical]
		internal static extern SafeMemoryMappedFileHandle OpenFileMapping(int dwDesiredAccess,
			[MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, string lpName);

		[DllImport(KERNEL32, SetLastError = true, ExactSpelling = true)]
		[SecurityCritical]
		internal static extern SafeMemoryMappedViewHandle MapViewOfFile(SafeMemoryMappedFileHandle handle,
			int dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, UIntPtr dwNumberOfBytesToMap);

		[DllImport(KERNEL32, SetLastError = true)]
		[SecurityCritical]
		internal static extern IntPtr VirtualAlloc(SafeMemoryMappedViewHandle address, UIntPtr numBytes,
			int commitOrReserve, int pageProtectionMode);

		#endregion

		[SecurityCritical]
		internal static bool GlobalMemoryStatusEx(ref MEMORYSTATUSEX lpBuffer) {
			lpBuffer.dwLength = (uint)Marshal.SizeOf(typeof(MEMORYSTATUSEX));
			return GlobalMemoryStatusExNative(ref lpBuffer);
		}

		[DllImport(KERNEL32, CharSet = CharSet.Auto, SetLastError = true, EntryPoint = "GlobalMemoryStatusEx")]
		[SecurityCritical]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool GlobalMemoryStatusExNative([In, Out] ref MEMORYSTATUSEX lpBuffer);

		[DllImport(KERNEL32, SetLastError = true)]
		[SecurityCritical]
		internal static extern unsafe bool CancelIoEx(SafeHandle handle, NativeOverlapped* lpOverlapped);

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		internal struct MEMORYSTATUSEX {
			internal uint dwLength;
			internal uint dwMemoryLoad;
			internal ulong ullTotalPhys;
			internal ulong ullAvailPhys;
			internal ulong ullTotalPageFile;
			internal ulong ullAvailPageFile;
			internal ulong ullTotalVirtual;
			internal ulong ullAvailVirtual;
			internal ulong ullAvailExtendedVirtual;
		}


	}
}

#endif