

using System.Runtime.Versioning;
#if !NET40Plus
using System;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Security;
using Microsoft.Win32.SafeHandles;

#if UNITY_SUBSET_NET35
using SharedMemory.Unofficial.System.Runtime.CompilerServices;
#else
using System.Runtime.CompilerServices;
#endif

namespace SharedMemory.Unofficial.System.Runtime.InteropServices {

	[SecurityCritical]
	public abstract unsafe class SafeBuffer : SafeHandleZeroOrMinusOneIsInvalid {

		private static readonly UIntPtr Uninitialized =
			UIntPtr.Size == 4 ? (UIntPtr) uint.MaxValue : (UIntPtr) ulong.MaxValue;

		private UIntPtr _numBytes;

		#region Constructors and initializers

		protected SafeBuffer(bool ownsHandle) : base(ownsHandle) => _numBytes = Uninitialized;

		/**
		 * <summary>
		 * Specifies the size of the region of memory, in bytes.  Must be 
		 * called before using the SafeBuffer.
		 * </summary>
		 * <param name="numBytes">Number of valid bytes in memory.</param>
		 */
		[CLSCompliant(false)]
		public void Initialize(ulong numBytes) {
			if (IntPtr.Size == 4 && numBytes > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(numBytes), "Value must be lesser than max memory space.");
			if (numBytes >= (ulong) Uninitialized)
				throw new ArgumentOutOfRangeException(nameof(numBytes), "Value must be lesser than UIntPtrMax value.");

			_numBytes = (UIntPtr) numBytes;
		}

		/**
		 * <summary>
		 * Specifies the the size of the region in memory, as the number of 
		 * elements in an array.  Must be called before using the SafeBuffer.
		 * </summary>
		 */
		[CLSCompliant(false)]
		public void Initialize(uint numElements, uint sizeOfEachElement) {
			if (IntPtr.Size == 4 && numElements * sizeOfEachElement > (ulong) uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(numElements), "Out of range address space.");
			if (numElements * sizeOfEachElement >= (ulong) Uninitialized)
				throw new ArgumentOutOfRangeException(nameof(numElements), "Out of range UIntPtrMax - 1.");

			_numBytes = checked((UIntPtr) (numElements * sizeOfEachElement));
		}

		/**
		 * <summary>
		 * Specifies the the size of the region in memory, as the number of 
		 * elements in an array.  Must be called before using the SafeBuffer.
		 * </summary>
		 */
		[CLSCompliant(false)]
		public void Initialize<T>(uint numElements) where T : struct {
			#if !UNITY_SUBSET_NET35
			Initialize(numElements, Marshal.AlignedSizeOf<T>());
			#else
			Initialize(numElements, AlignedSizeOf<T>());
			#endif
		}

		#endregion

		/**
		 * <summary>
		 * Obtain the pointer from a SafeBuffer for a block of code,
		 * with the express responsibility for bounds checking and calling 
		 * ReleasePointer later within a CER to ensure the pointer can be 
		 * freed later.  This method either completes successfully or 
		 * throws an exception and returns with pointer set to null.
		 * </summary>
		 * <param name="pointer">A byte*, passed by reference, to receive
		 * the pointer from within the SafeBuffer.  You must set
		 * pointer to null before calling this method.</param>
		 */
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void AcquirePointer(ref byte* pointer) {
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			var junk = false;
			DangerousAddRef(ref junk);
			pointer = (byte*) handle;
		}

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void ReleasePointer() {
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			DangerousRelease();
		}

		#region Public Methods 'Read/Write'

		/**
		 * <summary>
		 * Read a value type from memory at the given offset.  This is
		 * equivalent to:  return *(T*)(bytePtr + byteOffset);
		 * </summary>
		 * <typeparam name="T">The value type to read</typeparam>
		 * <param name="byteOffset">Where to start reading from memory.  You 
		 * may have to consider alignment.</param>
		 * <returns>An instance of T read from memory.</returns>
		 */
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public T Read<T>(ulong byteOffset) where T : struct {
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			#if !UNITY_SUBSET_NET35
			uint sizeofT = (uint) Marshal.SizeOf<T>();
			#else
			uint sizeofT = SizeOf<T>();
			#endif
			byte* ptr = (byte*) handle + byteOffset;
			SpaceCheck(ptr, sizeofT);

			T value;
			var mustCallRelease = false;
			try {
				DangerousAddRef(ref mustCallRelease);
				GenericPtrToStructure(ptr, out value, sizeofT);
			} finally {
				if (mustCallRelease)
					DangerousRelease();
			}

			return value;
		}

		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void ReadArray<T>(ulong byteOffset, T[] array, int index, int count) where T : struct {
			if (array == null)
				throw new ArgumentNullException(nameof(array), "Argument is null.");
			if (index < 0)
				throw new ArgumentOutOfRangeException(nameof(index), "Need non-negative number.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "Need non-negative number.");
			if (array.Length - index < count)
				throw new ArgumentException("Invalid offset length.");
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			#if !UNITY_SUBSET_NET35
			uint sizeofT = (uint) Marshal.SizeOf<T>();
			uint alignedSizeofT = Marshal.AlignedSizeOf<T>();
			#else
			uint sizeofT = SizeOf<T>();
			uint alignedSizeofT = AlignedSizeOf<T>();
			#endif
			byte* ptr = (byte*) handle + byteOffset;
			SpaceCheck(ptr, checked((ulong) (alignedSizeofT * count)));

			var mustCallRelease = false;
			try {
				DangerousAddRef(ref mustCallRelease);
				for (var i = 0; i < count; i++)
					GenericPtrToStructure(ptr + alignedSizeofT * i, out array[i + index], sizeofT);
			} finally {
				if (mustCallRelease)
					DangerousRelease();
			}
		}

		/**
		 * <summary>
		 * Write a value type to memory at the given offset.  This is
		 * equivalent to:  *(T*)(bytePtr + byteOffset) = value;
		 * </summary>
		 * <typeparam name="T">The type of the value type to write to memory.</typeparam>
		 * <param name="byteOffset">The location in memory to write to.  You 
		 * may have to consider alignment.</param>
		 * <param name="value">The value type to write to memory.</param>
		 */
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void Write<T>(ulong byteOffset, T value) where T : struct {
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			#if !UNITY_SUBSET_NET35
			uint sizeofT = (uint) Marshal.SizeOf<T>();
			#else
			uint sizeofT = SizeOf<T>();
			#endif
			byte* ptr = (byte*) handle + byteOffset;
			SpaceCheck(ptr, sizeofT);

			var mustCallRelease = false;
			try {
				DangerousAddRef(ref mustCallRelease);
				GenericStructureToPtr(ref value, ptr, sizeofT);
			} finally {
				if (mustCallRelease)
					DangerousRelease();
			}
		}

		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void WriteArray<T>(ulong byteOffset, T[] array, int index, int count) where T : struct {
			if (array == null)
				throw new ArgumentNullException(nameof(array), "Must be non-null.");
			if (index < 0)
				throw new ArgumentOutOfRangeException(nameof(index), "Need non-negative number.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "Need non-negative number.");
			if (array.Length - index < count)
				throw new ArgumentException("Invalid offset length.");
			if (_numBytes == Uninitialized)
				throw NotInitialized();

			#if !UNITY_SUBSET_NET35
			uint sizeofT = (uint) Marshal.SizeOf<T>();
			uint alignedSizeofT = Marshal.AlignedSizeOf<T>();
			#else
			uint sizeofT = SizeOf<T>();
			uint alignedSizeofT = AlignedSizeOf<T>();
			#endif
			byte* ptr = (byte*) handle + byteOffset;
			SpaceCheck(ptr, checked((ulong) (alignedSizeofT * count)));

			var mustCallRelease = false;
			try {
				DangerousAddRef(ref mustCallRelease);
				for (var i = 0; i < count; i++)
					GenericStructureToPtr(ref array[i + index], ptr + alignedSizeofT * i, sizeofT);
			} finally {
				if (mustCallRelease)
					DangerousRelease();
			}
		}

		#endregion

		/**
		 * <summary>
		 * Returns the number of bytes in the memory region.
		 * </summary>
		 */
		[CLSCompliant(false)]
		public ulong ByteLength {
			get {
				if (_numBytes == Uninitialized)
					throw NotInitialized();
				return (ulong) _numBytes;
			}
		}

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		private void SpaceCheck(byte* ptr, ulong sizeInBytes) {
			if ((ulong) _numBytes < sizeInBytes)
				NotEnoughRoom();
			if ((ulong) (ptr - (byte*) handle) > (ulong) _numBytes - sizeInBytes)
				NotEnoughRoom();
		}

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		private static void NotEnoughRoom() => throw new ArgumentException("Argument buffer is too small.");

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		private static InvalidOperationException NotInitialized() =>
			new InvalidOperationException("Must call Initialize() before any operations.");

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		internal static void GenericPtrToStructure<T>(byte* ptr, out T structure, uint sizeofT) where T : struct {
			structure = default;
			#if !UNITY_SUBSET_NET35
			PtrToStructureNative(ptr, __makeref(structure), sizeofT);
			#else
			fixed (byte* ptrStructure = &Unsafe.As<T, byte>(ref structure))
				Buffer.MemoryMove(ptrStructure, ptr, sizeofT);
			#endif
		}

		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		internal static void GenericStructureToPtr<T>(ref T structure, byte* ptr, uint sizeofT) where T : struct {
			#if !UNITY_SUBSET_NET35
			StructureToPtrNative(__makeref(structure), ptr, sizeofT);
			#else
			fixed (byte* ptrStructure = &Unsafe.As<T, byte>(ref structure))
				Buffer.MemoryMove(ptr, ptrStructure, sizeofT);
			#endif
		}

		#if !UNITY_SUBSET_NET35
		[MethodImpl(MethodImplOptions.InternalCall)]
		[ResourceExposure(ResourceScope.None)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		private static extern void PtrToStructureNative(byte* ptr, TypedReference structure, uint sizeofT);
		
		[MethodImpl(MethodImplOptions.InternalCall)]
		[ResourceExposure(ResourceScope.None)]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		private static extern void StructureToPtrNative(TypedReference structure, byte* ptr, uint sizeofT);
		#endif

		#if UNITY_SUBSET_NET35
		/**
		 * <summary>
		 * Returns the size that SafeBuffer (and hence, UnmanagedMemoryAccessor) reserves in the unmanaged buffer for each
		 * element of an array of T. This is not the same value that sizeof(T) returns! Since the primary use case is to
		 * parse memory mapped files, we cannot change this algorithm as this defines a de-facto serialization format.
		 * Throws if T contains GC references.
		 * </summary>
		 */
		internal static uint AlignedSizeOf<T>() where T : struct {
			uint size = SizeOf<T>();
			if (size == 1 || size == 2)
				return size;
			return (uint) (size + 3 & ~3);
		}
		#endif

		#if !UNITY_SUBSET_NET35
		#endif

		#if UNITY_SUBSET_NET35
		private static bool IsOrContainsRef(Type type) =>
			type.GetFields().Any(info => info.GetType().IsByRef && IsOrContainsRef(info.GetType()));

		private static bool IsOrContainsRef<T>() => IsOrContainsRef(typeof(T));

		/**
		 * <summary>
		 * Returns same value as sizeof(T) but throws if T contains GC references.
		 * </summary>
		 */
		private static uint SizeOf<T>() where T : struct {
			// if (RuntimeHelpers.IsReferenceOrContainsReferences<T>())
			if (IsOrContainsRef<T>())
				throw new ArgumentException("Need a structure with no references.");
			return Unsafe.SizeOf<T>();
		}
		#endif
	}
}

#endif