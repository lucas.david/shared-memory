﻿#if !NET40Plus && UNITY_SUBSET_NET35

using System;

namespace SharedMemory.Unofficial.System.IO {

	[Serializable]
	public enum HandleInheritability {
		None = 0,
		Inheritable = 1
	}

}

#endif