﻿#if !NET40Plus && UNITY_SUBSET_NET35

using System.Runtime.CompilerServices;

namespace SharedMemory.Unofficial.System.Runtime.CompilerServices {

	internal static class Unsafe {

		[MethodImpl(MethodImplOptions.ForwardRef)]
		internal static extern T As<T>(object o) where T : class;

		[MethodImpl(MethodImplOptions.ForwardRef)]
		internal static extern ref TTo As<TFrom, TTo>(ref TFrom source);

		[MethodImpl(MethodImplOptions.ForwardRef)]
		internal static extern uint SizeOf<T>();
	}
}

#endif