﻿#if !NET40Plus

#if BIT64
using nuint = System.UInt64;
#else
#endif

namespace SharedMemory.Unofficial.System {

	internal static class Buffer {
		internal static unsafe void ZeroMemory(byte[] src, long len) {
			fixed (byte* ptr = src)
				ZeroMemory(ptr, len);
		}

		internal static unsafe void ZeroMemory(byte* src, long len) {
			while (len-- > 0L)
				src[len] = 0;
		}

		internal static unsafe void MemoryCopy(byte* src, int srcIndex, byte* dest, int destIndex, int len) {
			if (len == 0)
				return;
			MemoryCopyImpl(dest + destIndex, src + srcIndex, (uint) len);
		}

		internal static unsafe void MemoryCopy(byte[] src, int srcIndex, byte* dest, int destIndex, int len) {
			if (len == 0)
				return;
			fixed (byte* srcPtr = src)
				MemoryCopyImpl(dest + destIndex, srcPtr + srcIndex, (uint) len);
		}

		internal static unsafe void MemoryCopy(byte* src, int srcIndex, byte[] dest, int destIndex, int len) {
			if (len == 0)
				return;
			fixed (byte* destPtr = dest)
				MemoryCopyImpl(destPtr + destIndex, src + srcIndex, (uint) len);
		}

		internal static unsafe void MemoryCopy(byte[] src, int srcIndex, byte[] dest, int destIndex, int len) {
			if (len == 0)
				return;
			fixed (byte* srcPtr = src, destPtr = dest)
				MemoryCopyImpl(destPtr + destIndex, srcPtr + srcIndex, (uint) len);
		}

		internal static unsafe void MemoryMove(byte* dest, byte* src, uint len) {
			if (dest == src)
				return;
			if (dest < src && src - dest < len) {
				var n = (uint) (src - dest + len);
				fixed (byte* ptr = new byte[n]) {
					MemoryCopyImpl(ptr, src, n); /* Bufferizes interpoled data. */
					MemoryCopyImpl(dest, src, n); /* Copies interpoled data. (left part) */
					MemoryCopyImpl(dest + n, src + n, len - n); /* Copies reminding data. (right part) */
				}

				return;
			}

			if (src < dest && dest - src < len) {
				var n = (uint) (src - dest + len);
				fixed (byte* ptr = new byte[n]) {
					MemoryCopyImpl(ptr, dest, n); /* Bufferize interpoled data. */
					MemoryCopyImpl(dest + n, src + n, n); /* Copies interpoled data. (right part) */
					MemoryCopyImpl(dest, src, len - n); /* Copies reminding data. (left part) */
				}

				return;
			}

			MemoryCopyImpl(dest, src, len);
		}

		private static unsafe void MemoryCopyImpl(byte* dest, byte* src, uint len) {
			while (len >= 64) {
				#if HAS_CUSTOM_BLOCKS
				*(Block64*) dest = *(Block64*) src;
				#elif BIT64
	            *(long*) dest = *(long*) src;
	            *(long*) (dest + 8) = *(long*) (src + 8);
				*(long*) (dest + 16) = *(long*) (src + 16);
	            *(long*) (dest + 24) = *(long*) (src + 24);
	            *(long*) (dest + 32) = *(long*) (src + 32);
	            *(long*) (dest + 40) = *(long*) (src + 40);
	            *(long*) (dest + 48) = *(long*) (src + 48);
	            *(long*) (dest + 56) = *(long*) (src + 56);
				#else
				*(int*) dest = *(int*) src;
				*(int*) (dest + 4) = *(int*) (src + 4);
				*(int*) (dest + 8) = *(int*) (src + 8);
				*(int*) (dest + 12) = *(int*) (src + 12);
				*(int*) (dest + 16) = *(int*) (src + 16);
				*(int*) (dest + 20) = *(int*) (src + 20);
				*(int*) (dest + 24) = *(int*) (src + 24);
				*(int*) (dest + 28) = *(int*) (src + 28);
				*(int*) (dest + 32) = *(int*) (src + 32);
				*(int*) (dest + 36) = *(int*) (src + 36);
				*(int*) (dest + 40) = *(int*) (src + 40);
				*(int*) (dest + 44) = *(int*) (src + 44);
				*(int*) (dest + 48) = *(int*) (src + 48);
				*(int*) (dest + 52) = *(int*) (src + 52);
				*(int*) (dest + 56) = *(int*) (src + 56);
				*(int*) (dest + 60) = *(int*) (src + 60);
				#endif
				dest += 64;
				src += 64;
				len -= 64;
			}

			if (len >= 32) {
				#if HAS_CUSTOM_BLOCKS
	            *(Block16*) dest = *(Block16*) src;
				*(Block16*) (dest + 16) = *(Block16*) (src + 16);
				#elif BIT64
	            *(long*) dest = *(long*) src;
	            *(long*) (dest + 8) = *(long*) (src + 8);
				*(long*) (dest + 16) = *(long*) (src + 16);
	            *(long*) (dest + 24) = *(long*) (src + 24);
				#else
				*(int*) dest = *(int*) src;
				*(int*) (dest + 4) = *(int*) (src + 4);
				*(int*) (dest + 8) = *(int*) (src + 8);
				*(int*) (dest + 12) = *(int*) (src + 12);
				*(int*) (dest + 16) = *(int*) (src + 16);
				*(int*) (dest + 20) = *(int*) (src + 20);
				*(int*) (dest + 24) = *(int*) (src + 24);
				*(int*) (dest + 28) = *(int*) (src + 28);
				#endif
				dest += 32;
				src += 32;
				len -= 32;
			}

			if (len >= 16) {
				#if HAS_CUSTOM_BLOCKS
		        *(Block16*) dest = *(Block16*) src;
				#elif BIT64
				*(long*) dest = *(long*) src;
	            *(long*) (dest + 8) = *(long*) (src + 8);
				#else
				*(int*) dest = *(int*) src;
				*(int*) (dest + 4) = *(int*) (src + 4);
				*(int*) (dest + 8) = *(int*) (src + 8);
				*(int*) (dest + 12) = *(int*) (src + 12);
				#endif
				dest += 16;
				src += 16;
				len -= 16;
			}

			if (len >= 8) {
				#if BIT64
				*(long*) dest = *(long*) src;
				#else
				*(int*) dest = *(int*) src;
				*(int*) (dest + 4) = *(int*) (src + 4);
				#endif
				dest += 8;
				src += 8;
				len -= 8;
			}

			if (len >= 4) {
				*(int*) dest = *(int*) src;
				dest += 4;
				src += 4;
				len -= 4;
			}

			if (len >= 2) {
				*(short*) dest = *(short*) src;
				dest += 2;
				src += 2;
				len -= 2;
			}

			if (len == 1)
				*dest = *src;
		}

		#if HAS_CUSTOM_BLOCKS
		[StructLayout(LayoutKind.Sequential, Size = 16)]
		private struct Block16 { }

		[StructLayout(LayoutKind.Sequential, Size = 64)]
		private struct Block64 { }
		#endif

	}

}

#endif