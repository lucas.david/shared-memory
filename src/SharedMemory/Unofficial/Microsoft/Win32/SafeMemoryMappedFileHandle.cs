#if !NET40Plus

using System;
using System.Runtime.Versioning;
using System.Security.Permissions;
using SharedMemory.Unofficial.Microsoft.Win32.SafeHandles;
using SharedMemory.Unofficial.System.Runtime.InteropServices;

namespace SharedMemory.Unofficial.Microsoft.Win32 {

	public sealed class SafeMemoryMappedFileHandle : SafeBuffer {

		[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
		internal SafeMemoryMappedFileHandle() : base(true) { }

		[SecurityPermission(SecurityAction.LinkDemand, UnmanagedCode = true)]
		internal SafeMemoryMappedFileHandle(IntPtr handle, bool ownsHandle) : base(ownsHandle) => SetHandle(handle);

		[ResourceExposure(ResourceScope.Machine)]
		[ResourceConsumption(ResourceScope.Machine)]
		protected override bool ReleaseHandle() => UnsafeNativeMethods.CloseHandle(handle);
	}
}

#endif