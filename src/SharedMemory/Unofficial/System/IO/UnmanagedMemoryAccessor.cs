#if !NET40Plus

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Permissions;
using SharedMemory.Unofficial.System.Runtime.InteropServices;

namespace SharedMemory.Unofficial.System.IO {

	public class UnmanagedMemoryAccessor : IDisposable {
		private FileAccess _access;

		[SecurityCritical] private SafeBuffer _buffer;
		private bool _canRead;
		private bool _canWrite;
		private long _capacity;
		private bool _isOpen;

		private long _offset;

		protected UnmanagedMemoryAccessor() => _isOpen = false;

		public long Capacity() => _capacity;

		public bool CanRead() => _isOpen && _canRead;

		public bool CanWrite() => _isOpen && _canWrite;

		protected bool IsOpen() => _isOpen;

		#region Constructors and initializers

		[SecuritySafeCritical]
		public UnmanagedMemoryAccessor(SafeBuffer buffer, long offset, long capacity) =>
			Initialize(buffer, offset, capacity, FileAccess.Read);

		[SecuritySafeCritical]
		public UnmanagedMemoryAccessor(SafeBuffer buffer, long offset, long capacity, FileAccess access) =>
			Initialize(buffer, offset, capacity, access);

		[SecuritySafeCritical]
		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		protected void Initialize(SafeBuffer buffer, long offset, long capacity, FileAccess access) {
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "must be larger than or equal to 0.");
			if (capacity < 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "must be larger than or equal to 0.");
			if (buffer.ByteLength < (ulong) (offset + capacity))
				throw new ArgumentException("offset plus capacity must be smaller than buffer length.");
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
				throw new ArgumentOutOfRangeException(nameof(access));

			if (_isOpen)
				throw new InvalidOperationException("Initialization called twice.");

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					buffer.AcquirePointer(ref pointer);
					if ((byte*) ((long) pointer + offset + capacity) < pointer)
						throw new ArgumentException("Offset and capacity are out of bounds.");
				} finally {
					if (pointer != null)
						buffer.ReleasePointer();
				}
			}

			_offset = offset;
			_buffer = buffer;
			_capacity = capacity;
			_access = access;
			_isOpen = true;
			_canRead = (_access & FileAccess.Read) != 0;
			_canWrite = (_access & FileAccess.Write) != 0;
		}

		#endregion Constructors and initializers

		#region Disposable Pattern

		protected virtual void Dispose(bool disposing) => _isOpen = false;

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

		#region Public Methods 'Read'

		public bool ReadBoolean(long position) {
			const int sizeOfType = sizeof(bool);
			EnsureSafeToRead(position, sizeOfType);
			byte b = InternalReadByte(position);
			return b != 0;
		}

		public byte ReadByte(long position) {
			const int sizeOfType = sizeof(byte);
			EnsureSafeToRead(position, sizeOfType);
			return InternalReadByte(position);
		}

		[SecuritySafeCritical]
		public char ReadChar(long position) {
			const int sizeOfType = sizeof(char);
			EnsureSafeToRead(position, sizeOfType);

			char result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(char*) pointer;
					#if ALIGN_ACCESS
					else result = (char) (*pointer | *(pointer + 1) << 8);
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public short ReadInt16(long position) {
			const int sizeOfType = sizeof(short);
			EnsureSafeToRead(position, sizeOfType);

			short result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(short*) pointer;
					#if ALIGN_ACCESS
					else result = (short) (*pointer | *(pointer + 1) << 8);
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public int ReadInt32(long position) {
			const int sizeOfType = sizeof(int);
			EnsureSafeToRead(position, sizeOfType);

			int result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(int*) pointer;
					#if ALIGN_ACCESS
					else
						result = *pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 | *(pointer + 3) << 24;
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public long ReadInt64(long position) {
			const int sizeOfType = sizeof(long);
			EnsureSafeToRead(position, sizeOfType);

			long result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(long*) pointer;
					#if ALIGN_ACCESS
					else {
						int lo = *pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 | *(pointer + 3) << 24;
						int hi = *(pointer + 4) | *(pointer + 5) << 8 | *(pointer + 6) << 16 | *(pointer + 7) << 24;
						result = (long) hi << 32 | lo;
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public decimal ReadDecimal(long position) {
			const int sizeOfType = sizeof(decimal);
			EnsureSafeToRead(position, sizeOfType);

			var decimalArray = new int[4];
			ReadArray(position, decimalArray, 0, decimalArray.Length);

			return new decimal(decimalArray);
		}

		[SecuritySafeCritical]
		public float ReadSingle(long position) {
			const int sizeOfType = sizeof(float);
			EnsureSafeToRead(position, sizeOfType);

			float result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;

					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(float*) pointer;
					#if ALIGN_ACCESS
					else {
						var tempResult = (uint) (*pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 |
							*(pointer + 3) << 24);
						result = *(float*) &tempResult;
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public double ReadDouble(long position) {
			const int sizeOfType = sizeof(double);
			EnsureSafeToRead(position, sizeOfType);

			double result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;

					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(double*) pointer;
					#if ALIGN_ACCESS
					else {
						uint lo = (uint) (*pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 | *(pointer + 3) << 24);
						uint hi = (uint) (*(pointer + 4) | *(pointer + 5) << 8 | *(pointer + 6) << 16 |
							*(pointer + 7) << 24);
						UInt64 tempResult = (ulong) hi << 32 | lo;
						result = *(double*) &tempResult;
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public sbyte ReadSByte(long position) {
			const int sizeOfType = sizeof(sbyte);
			EnsureSafeToRead(position, sizeOfType);

			sbyte result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					result = *(sbyte*) pointer;
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public ushort ReadUInt16(long position) {
			const int sizeOfType = sizeof(ushort);
			EnsureSafeToRead(position, sizeOfType);

			ushort result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;

					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(ushort*) pointer;
					#if ALIGN_ACCESS
					else
						result = (ushort) (*pointer | *(pointer + 1) << 8);
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public uint ReadUInt32(long position) {
			const int sizeOfType = sizeof(uint);
			EnsureSafeToRead(position, sizeOfType);

			uint result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(uint*) pointer;
					#if ALIGN_ACCESS
					else
						result = (uint) (*pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 | *(pointer + 3) << 24);
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		public ulong ReadUInt64(long position) {
			const int sizeOfType = sizeof(ulong);
			EnsureSafeToRead(position, sizeOfType);

			ulong result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					result = *(ulong*) pointer;
					#if ALIGN_ACCESS
					else {
						var lo = (uint) (*pointer | *(pointer + 1) << 8 | *(pointer + 2) << 16 | *(pointer + 3) << 24);
						var hi = (uint) (*(pointer + 4) | *(pointer + 5) << 8 | *(pointer + 6) << 16 |
							*(pointer + 7) << 24);
						result = (ulong) hi << 32 | lo;
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecurityCritical]
		public void Read<T>(long position, out T structure) where T : struct {
			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "must be larger than or equal to 0.");

			if (!_isOpen)
				throw new ObjectDisposedException(GetType().Name, "Object is already closed.");

			if (!_canRead)
				throw new NotSupportedException("Reading is not supported.");

			var sizeOfT = (uint) Marshal.SizeOf<T>();
			if (position > _capacity - sizeOfT) {
				if (position >= _capacity)
					throw new ArgumentOutOfRangeException(nameof(position), "must be smaller than capacity set.");
				throw new ArgumentException("Not enough bytes to read", nameof(position));
			}

			structure = _buffer.Read<T>((ulong) (_offset + position));
		}

		[SecurityCritical]
		public int ReadArray<T>(long position, T[] array, int offset, int count) where T : struct {
			if (array == null)
				throw new ArgumentNullException(nameof(array), "must not be null.");
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "must be larger than or equal to 0.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "must be larger than or equal to 0.");
			if (array.Length - offset < count)
				throw new ArgumentException("Offset and length are out of bounds.");
			if (!_canRead) {
				if (!_isOpen)
					throw new ObjectDisposedException(GetType().Name, "Object is already closed.");
				throw new NotSupportedException("Reading is not supported.");
			}

			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "must be larger than or equal to 0.");

			uint sizeOfT = Marshal.AlignedSizeOf<T>();

			if (position >= _capacity)
				throw new ArgumentOutOfRangeException(nameof(position), "must be less than capacity.");

			int n = count;
			long spaceLeft = _capacity - position;
			if (spaceLeft < 0)
				n = 0;
			else {
				var spaceNeeded = (ulong) (sizeOfT * count);
				if ((ulong) spaceLeft < spaceNeeded)
					n = (int) (spaceLeft / sizeOfT);
			}

			_buffer.ReadArray((ulong) (_offset + position), array, offset, n);

			return n;
		}

		#endregion

		#region Public Methods 'Write'

		public void Write(long position, bool value) {
			const int sizeOfType = sizeof(bool);
			EnsureSafeToWrite(position, sizeOfType);

			var b = (byte) (value ? 1 : 0);
			InternalWrite(position, b);
		}

		public void Write(long position, byte value) {
			const int sizeOfType = sizeof(byte);
			EnsureSafeToWrite(position, sizeOfType);

			InternalWrite(position, value);
		}

		[SecuritySafeCritical]
		public void Write(long position, char value) {
			const int sizeOfType = sizeof(char);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(char*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
					}
					#endif
				} finally {
					if (pointer != null) _buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, short value) {
			const int sizeOfType = sizeof(short);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(short*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, int value) {
			const int sizeOfType = sizeof(int);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(int*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
						*(pointer + 2) = (byte) (value >> 16);
						*(pointer + 3) = (byte) (value >> 24);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, long value) {
			const int sizeOfType = sizeof(long);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(long*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
						*(pointer + 2) = (byte) (value >> 16);
						*(pointer + 3) = (byte) (value >> 24);
						*(pointer + 4) = (byte) (value >> 32);
						*(pointer + 5) = (byte) (value >> 40);
						*(pointer + 6) = (byte) (value >> 48);
						*(pointer + 7) = (byte) (value >> 56);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, decimal value) {
			const int sizeOfType = sizeof(decimal);
			EnsureSafeToWrite(position, sizeOfType);

			var decimalArray = new byte[16];
			Decimal.GetBytes(value, decimalArray);

			var bits = new int[4];
			int flags = decimalArray[12] | decimalArray[13] << 8 | decimalArray[14] << 16 | decimalArray[15] << 24;
			int lo = decimalArray[0] | decimalArray[1] << 8 | decimalArray[2] << 16 | decimalArray[3] << 24;
			int mid = decimalArray[4] | decimalArray[5] << 8 | decimalArray[6] << 16 | decimalArray[7] << 24;
			int hi = decimalArray[8] | decimalArray[9] << 8 | decimalArray[10] << 16 | decimalArray[11] << 24;

			bits[0] = lo;
			bits[1] = mid;
			bits[2] = hi;
			bits[3] = flags;

			WriteArray(position, bits, 0, bits.Length);
		}

		[SecuritySafeCritical]
		public void Write(long position, float value) {
			const int sizeOfType = sizeof(float);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(float*) pointer = value;
					#if ALIGN_ACCESS
					else {
						uint tmpValue = *(uint*) &value;
						*pointer = (byte) tmpValue;
						*(pointer + 1) = (byte) (tmpValue >> 8);
						*(pointer + 2) = (byte) (tmpValue >> 16);
						*(pointer + 3) = (byte) (tmpValue >> 24);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, double value) {
			const int sizeOfType = sizeof(double);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(double*) pointer = value;
					#if ALIGN_ACCESS
					else {
						ulong tmpValue = *(ulong*) &value;
						*pointer = (byte) tmpValue;
						*(pointer + 1) = (byte) (tmpValue >> 8);
						*(pointer + 2) = (byte) (tmpValue >> 16);
						*(pointer + 3) = (byte) (tmpValue >> 24);
						*(pointer + 4) = (byte) (tmpValue >> 32);
						*(pointer + 5) = (byte) (tmpValue >> 40);
						*(pointer + 6) = (byte) (tmpValue >> 48);
						*(pointer + 7) = (byte) (tmpValue >> 56);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, sbyte value) {
			const int sizeOfType = sizeof(sbyte);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					*(sbyte*) pointer = value;
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, ushort value) {
			const int sizeOfType = sizeof(ushort);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(ushort*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecuritySafeCritical]
		public void Write(long position, uint value) {
			const int sizeOfType = sizeof(uint);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(uint*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
						*(pointer + 2) = (byte) (value >> 16);
						*(pointer + 3) = (byte) (value >> 24);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		public void Write(long position, ulong value) {
			const int sizeOfType = sizeof(ulong);
			EnsureSafeToWrite(position, sizeOfType);

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					pointer += _offset + position;
					#if ALIGN_ACCESS
					if (((int) pointer & sizeOfType - 1) == 0)
					#endif
					*(ulong*) pointer = value;
					#if ALIGN_ACCESS
					else {
						*pointer = (byte) value;
						*(pointer + 1) = (byte) (value >> 8);
						*(pointer + 2) = (byte) (value >> 16);
						*(pointer + 3) = (byte) (value >> 24);
						*(pointer + 4) = (byte) (value >> 32);
						*(pointer + 5) = (byte) (value >> 40);
						*(pointer + 6) = (byte) (value >> 48);
						*(pointer + 7) = (byte) (value >> 56);
					}
					#endif
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		[SecurityCritical]
		public void Write<T>(long position, ref T structure) where T : struct {
			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "must be larger than or equal to 0.");
			if (!_isOpen)
				throw new ObjectDisposedException(GetType().Name, "Object is already closed.");
			if (!_canWrite)
				throw new NotSupportedException("Writing is not supported.");

			var sizeOfT = (uint) Marshal.SizeOf<T>();
			if (position > _capacity - sizeOfT) {
				if (position >= _capacity)
					throw new ArgumentOutOfRangeException(nameof(position), "must be less than capacity.");
				throw new ArgumentException(string.Format("Not enough bytes to write {0}", typeof(T).Name),
					nameof(position));
			}

			_buffer.Write((ulong) (_offset + position), structure);
		}

		[SecurityCritical]
		public void WriteArray<T>(long position, T[] array, int offset, int count) where T : struct {
			if (array == null)
				throw new ArgumentNullException(nameof(array), "Buffer cannot be null.");
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "must be larger than or equal to 0.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "must be larger than or equal to 0.");
			if (array.Length - offset < count)
				throw new ArgumentException("Offset and length are out of bounds.");
			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "must be larger than or equal to 0.");
			if (position >= _capacity)
				throw new ArgumentOutOfRangeException(nameof(position), "must be less than capacity.");
			if (!_isOpen)
				throw new ObjectDisposedException(GetType().Name, "Object is already closed.");
			if (!_canWrite)
				throw new NotSupportedException("Not supported writing.");

			_buffer.WriteArray((ulong) (_offset + position), array, offset, count);
		}

		#endregion

		#region Private Methods

		[SecuritySafeCritical]
		private byte InternalReadByte(long position) {
			Debug.Assert(_canRead, "UMA not readable");
			Debug.Assert(position >= 0, "position less than 0");
			Debug.Assert(position <= _capacity - sizeof(byte), "position is greater than capacity - sizeof(byte)");

			byte result;
			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					result = *(pointer + _offset + position);
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}

			return result;
		}

		[SecuritySafeCritical]
		private void InternalWrite(long position, byte value) {
			Debug.Assert(_canWrite, "UMA not writable");
			Debug.Assert(position >= 0, "position less than 0");
			Debug.Assert(position <= _capacity - sizeof(byte), "position is greater than capacity - sizeof(byte)");

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					_buffer.AcquirePointer(ref pointer);
					*(pointer + _offset + position) = value;
				} finally {
					if (pointer != null)
						_buffer.ReleasePointer();
				}
			}
		}

		private void EnsureSafeToRead(long position, int sizeOfType) {
			if (!_isOpen)
				throw new ObjectDisposedException(GetType().Name, "Object is already closed.");
			if (!_canRead)
				throw new NotSupportedException("Reading not supported.");
			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "Must be larger than or equal to 0.");
			if (position > _capacity - sizeOfType) {
				if (position >= _capacity)
					throw new ArgumentOutOfRangeException(nameof(position), "Must be smaller than capacity set.");
				throw new ArgumentException("Not enough bytes to read.", nameof(position));
			}
		}

		private void EnsureSafeToWrite(long position, int sizeOfType) {
			if (!_isOpen)
				throw new ObjectDisposedException(GetType().Name, "Object is already close.");
			if (!_canWrite)
				throw new NotSupportedException("Reading not supported.");
			if (position < 0)
				throw new ArgumentOutOfRangeException(nameof(position), "Must be larger than or equal to 0.");
			if (position > _capacity - sizeOfType) {
				if (position >= _capacity)
					throw new ArgumentOutOfRangeException(nameof(position), "Must be smaller than capacity set.");
				throw new ArgumentException("Not enough bytes to read.", nameof(position));
			}
		}

		#endregion

	}
}

#endif