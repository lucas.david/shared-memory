#if !NET40Plus

using System;
using System.Diagnostics;
using System.Security;
using System.Security.Permissions;
using SharedMemory.Unofficial.Microsoft.Win32;

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {

	public sealed class MemoryMappedViewAccessor : UnmanagedMemoryAccessor {

		private readonly MemoryMappedView _view;

		[SecurityCritical]
		internal MemoryMappedViewAccessor(MemoryMappedView view) {
			Debug.Assert(view != null, "view is null");

			_view = view;
			Initialize(_view.ViewHandle, _view.PointerOffset, _view.Size, MemoryMappedFile.GetFileAccess(_view.Access));
		}

		public SafeMemoryMappedViewHandle SafeMemoryMappedViewHandle {
			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get => _view?.ViewHandle;
		}

		public long PointerOffset {
			get {
				if (_view == null)
					throw new InvalidOperationException();
				return _view.PointerOffset;
			}
		}

		#region Disposable Pattern

		[SecuritySafeCritical]
		protected override void Dispose(bool disposing) {
			try {
				if (disposing && _view != null && !_view.IsClosed)
					Flush();
			} finally {
				try {
					_view?.Dispose();
				} finally {
					base.Dispose(disposing);
				}
			}
		}

		#endregion

		[SecurityCritical]
		public void Flush() {
			if (!IsOpen())
				throw new ObjectDisposedException(GetType().Name);

			_view?.Flush((IntPtr) Capacity());
		}
	}
}

#endif