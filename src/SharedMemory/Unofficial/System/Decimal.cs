﻿#if !NET40Plus

using System.Diagnostics;

namespace SharedMemory.Unofficial.System {
	public static class Decimal {

		public static void GetBytes(decimal d, byte[] buffer) {
			Debug.Assert(buffer != null && buffer.Length >= 16, "buffer != null && buffer.Length >= 16");

			int[] bits = decimal.GetBits(d);
			int lo = bits[0], mid = bits[1], hi = bits[2], flags = bits[3];

			buffer[0] = (byte) lo;
			buffer[1] = (byte) (lo >> 8);
			buffer[2] = (byte) (lo >> 16);
			buffer[3] = (byte) (lo >> 24);

			buffer[4] = (byte) mid;
			buffer[5] = (byte) (mid >> 8);
			buffer[6] = (byte) (mid >> 16);
			buffer[7] = (byte) (mid >> 24);

			buffer[8] = (byte) hi;
			buffer[9] = (byte) (hi >> 8);
			buffer[10] = (byte) (hi >> 16);
			buffer[11] = (byte) (hi >> 24);

			buffer[12] = (byte) flags;
			buffer[13] = (byte) (flags >> 8);
			buffer[14] = (byte) (flags >> 16);
			buffer[15] = (byte) (flags >> 24);
		}
	}
}

#endif