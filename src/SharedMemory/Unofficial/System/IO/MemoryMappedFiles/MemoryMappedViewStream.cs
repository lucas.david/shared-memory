#if !NET40Plus

using System;
using System.Diagnostics;
using System.IO;
using System.Security;
using System.Security.Permissions;
using SharedMemory.Unofficial.Microsoft.Win32;

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {

	public sealed class MemoryMappedViewStream : UnmanagedMemoryStream {

		private readonly MemoryMappedView m_view;

		[SecurityCritical]
		internal MemoryMappedViewStream(MemoryMappedView view) {
			Debug.Assert(view != null, "view is null");

			m_view = view;
			Initialize(m_view.ViewHandle, m_view.PointerOffset, m_view.Size,
				MemoryMappedFile.GetFileAccess(m_view.Access));
		}

		public SafeMemoryMappedViewHandle SafeMemoryMappedViewHandle {

			[SecurityCritical]
			[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
			get => m_view?.ViewHandle;
		}

		public long PointerOffset {
			get {
				if (m_view == null)
					throw new InvalidOperationException();
				return m_view.PointerOffset;
			}
		}

		public override void SetLength(long value) => throw new NotSupportedException();

		[SecuritySafeCritical]
		protected override void Dispose(bool disposing) {
			try {
				if (disposing && m_view != null && !m_view.IsClosed)
					Flush();
			} finally {
				try {
					m_view?.Dispose();
				} finally {
					base.Dispose(disposing);
				}
			}
		}

		[SecurityCritical]
		public override void Flush() {
			if (!CanSeek)
				throw new IOException("Stream is closed.");

			m_view?.Flush((IntPtr) Capacity);
		}
	}
}

#endif