using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using SharedMemory.Unofficial.Microsoft.Win32;
using SharedMemory.Unofficial.Microsoft.Win32.SafeHandles;
#if !NET40Plus

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {

	internal class MemoryMappedView : IDisposable {

		private const int MaxFlushWaits = 15; /* must be <=30 */
		private const int MaxFlushRetriesPerWait = 20;

		private SafeMemoryMappedViewHandle _viewHandle;
		private long _pointerOffset;
		private long _size;
		private MemoryMappedFileAccess _access;

		[SecurityCritical]
		private MemoryMappedView(SafeMemoryMappedViewHandle viewHandle, long pointerOffset, long size,
			MemoryMappedFileAccess access) {
			_viewHandle = viewHandle;
			_pointerOffset = pointerOffset;
			_size = size;
			_access = access;
		}

		internal SafeMemoryMappedViewHandle ViewHandle {
			[SecurityCritical]
			get {
				return _viewHandle;
			}
		}

		internal long PointerOffset {
			get {
				return _pointerOffset;
			}
		}

		internal long Size {
			get {
				return _size;
			}
		}

		internal MemoryMappedFileAccess Access {
			get {
				return _access;
			}
		}

		internal bool IsClosed {
			[SecuritySafeCritical]
			get {
				return _viewHandle == null || _viewHandle.IsClosed;
			} 
		}

		[SecurityCritical]
		internal static MemoryMappedView CreateView(SafeMemoryMappedFileHandle memMappedFileHandle,
			MemoryMappedFileAccess access, long offset, long size) {
			ulong extraMemNeeded = (ulong) offset % (ulong) MemoryMappedFile.GetSystemPageAllocationGranularity();

			ulong newOffset = (ulong) offset - extraMemNeeded;
			Debug.Assert(offset - (long) extraMemNeeded >= 0, "newOffset = (offset - extraMemNeeded) < 0");

			ulong nativeSize;
			if (size != MemoryMappedFile.DefaultSize)
				nativeSize = (ulong) size + extraMemNeeded;
			else
				nativeSize = 0;

			if (IntPtr.Size == 4 && nativeSize > uint.MaxValue)
				throw new ArgumentOutOfRangeException(nameof(size));

			var memStatus = new UnsafeNativeMethods.MEMORYSTATUSEX();
			UnsafeNativeMethods.GlobalMemoryStatusEx(ref memStatus);
			ulong totalVirtual = memStatus.ullTotalVirtual;
			if (nativeSize >= totalVirtual)
				throw new IOException();

			var offsetLow = (uint) (newOffset & 0x00000000FFFFFFFFL);
			var offsetHigh = (uint) (newOffset >> 32);

			SafeMemoryMappedViewHandle viewHandle = UnsafeNativeMethods.MapViewOfFile(memMappedFileHandle,
				MemoryMappedFile.GetFileMapAccess(access), offsetHigh, offsetLow, new UIntPtr(nativeSize));
			if (viewHandle.IsInvalid)
				throw new IOException(UnsafeNativeMethods.GetMessage(Marshal.GetLastWin32Error()));

			var viewInfo = new UnsafeNativeMethods.MEMORY_BASIC_INFORMATION();
			UnsafeNativeMethods.VirtualQuery(viewHandle, ref viewInfo, (IntPtr) Marshal.SizeOf(viewInfo));
			var viewSize = (ulong) viewInfo.RegionSize;

			if ((viewInfo.State & UnsafeNativeMethods.MEM_RESERVE) != 0 || viewSize < nativeSize) {
				ulong allocSize = nativeSize == 0 ? viewSize : nativeSize;
				UnsafeNativeMethods.VirtualAlloc(viewHandle, (UIntPtr) allocSize, UnsafeNativeMethods.MEM_COMMIT,
					MemoryMappedFile.GetPageAccess(access));
				Marshal.GetLastWin32Error();

				viewInfo = new UnsafeNativeMethods.MEMORY_BASIC_INFORMATION();
				UnsafeNativeMethods.VirtualQuery(viewHandle, ref viewInfo, (IntPtr) Marshal.SizeOf(viewInfo));
				viewSize = (ulong) viewInfo.RegionSize;
			}

			if (size == MemoryMappedFile.DefaultSize)
				size = (long) (viewSize - extraMemNeeded);
			else
				Debug.Assert(viewSize >= (ulong) size, "viewSize < size");

			viewHandle.Initialize((ulong) size + extraMemNeeded);

			return new MemoryMappedView(viewHandle, (long) extraMemNeeded, size, access);
		}

		[SecurityCritical]
		public void Flush(IntPtr capacity) {
			if (ViewHandle != null)
				unsafe {
					byte* firstPagePtr = null;
					RuntimeHelpers.PrepareConstrainedRegions();
					try {
						ViewHandle.AcquirePointer(ref firstPagePtr);

						bool success = UnsafeNativeMethods.FlushViewOfFile(firstPagePtr, capacity);
						if (success)
							return;

						int error = Marshal.GetLastWin32Error();
						bool canRetry = error == UnsafeNativeMethods.ERROR_LOCK_VIOLATION;

						for (var w = 0; canRetry && w < MaxFlushWaits; w++) {
							int pause = 1 << w;
							Thread.Sleep(pause);

							for (var r = 0; canRetry && r < MaxFlushRetriesPerWait; r++) {
								success = UnsafeNativeMethods.FlushViewOfFile(firstPagePtr, capacity);
								if (success)
									return;

								Thread.Sleep(0);

								error = Marshal.GetLastWin32Error();
								canRetry = error == UnsafeNativeMethods.ERROR_LOCK_VIOLATION;
							}
						}

						throw new IOException(UnsafeNativeMethods.GetMessage(error));
					} finally {
						if (firstPagePtr != null)
							ViewHandle.ReleasePointer();
					}
				}
		}

		#region Disposable Pattern

		[SecurityCritical]
		protected virtual void Dispose(bool disposing) {
			if (ViewHandle != null && !ViewHandle.IsClosed)
				_viewHandle.Dispose();
		}

		[SecurityCritical]
		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

	}
}

#endif