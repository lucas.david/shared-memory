#if !NET40Plus

using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Threading;
using SharedMemory.Unofficial.System.Runtime.InteropServices;

namespace SharedMemory.Unofficial.System.IO {

	public class UnmanagedMemoryStream : Stream {
		private const long UnmanagedMemStreamMaxLength = long.MaxValue;
		private FileAccess _access;

		[SecurityCritical] private SafeBuffer _buffer;
		private long _capacity;
		private bool _isOpen;
		private long _length;

		[SecurityCritical] private unsafe byte* _mem;
		private long _offset;
		private long _position;

		public override bool CanRead => _isOpen && (_access & FileAccess.Read) != 0;

		public override bool CanSeek => _isOpen;

		public override bool CanWrite => _isOpen && (_access & FileAccess.Write) != 0;

		public override long Length {
			get {
				if (!_isOpen)
					throw new IOException("Stream is closed.");
				return Interlocked.Read(ref _length);
			}
		}

		public long Capacity {
			get {
				if (!_isOpen)
					throw new IOException("Stream is closed.");
				return _capacity;
			}
		}

		public override long Position {
			get {
				if (!CanSeek)
					throw new IOException("Stream is closed.");
				return Interlocked.Read(ref _position);
			}
			[SecuritySafeCritical]
			set {
				if (value < 0)
					throw new ArgumentOutOfRangeException(nameof(value), "Need non-negative number.");
				if (!CanSeek)
					throw new IOException("Stream is closed.");

				#if WIN32
                unsafe {
                    if (value > (long) Int32.MaxValue || _mem + value < _mem)
                        throw new ArgumentOutOfRangeException("value", Environment.GetResourceString("ArgumentOutOfRange_StreamLength"));
                }
				#endif
				Interlocked.Exchange(ref _position, value);
			}
		}

		public unsafe byte* PositionPointer {
			[SecurityCritical]
			get {
				if (_buffer != null)
					throw new NotSupportedException("UmsSafeBuffer");
				long pos = Interlocked.Read(ref _position);
				if (pos > _capacity)
					throw new IndexOutOfRangeException("UMSPosition");
				byte* ptr = _mem + pos;
				if (!_isOpen)
					throw new IOException("Stream is closed.");
				return ptr;
			}
			[SecurityCritical]
			set {
				if (_buffer != null)
					throw new NotSupportedException("UmsSafeBuffer");
				if (!_isOpen)
					throw new IOException("Stream is closed.");
				if ((ulong) new IntPtr(value - _mem).ToInt64() > UnmanagedMemStreamMaxLength)
					throw new ArgumentOutOfRangeException("offset", "UnmanagedMemStreamLength");
				if (value < _mem)
					throw new IOException("SeekBeforeBegin");
				Interlocked.Exchange(ref _position, value - _mem);
			}
		}

		internal unsafe byte* Pointer {
			[SecurityCritical]
			get {
				if (_buffer != null)
					throw new NotSupportedException("UmsSafeBuffer");
				return _mem;
			}
		}

		[SecuritySafeCritical]
		protected override void Dispose(bool disposing) {
			_isOpen = false;
			unsafe {
				_mem = null;
			}

			base.Dispose(disposing);
		}

		public override void Flush() {
			if (!_isOpen)
				throw new IOException("Stream is closed.");
		}

		[SecuritySafeCritical]
		public override int Read([In] [Out] byte[] buffer, int offset, int count) {
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer), "Must be not null.");
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "Need non-negative number.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "Need non-negative number.");
			if (buffer.Length - offset < count)
				throw new ArgumentException("Invalid offset and length.");
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if (!CanRead)
				throw new IOException("Reading not supported.");

			long pos = Interlocked.Read(ref _position);
			long len = Interlocked.Read(ref _length);
			long n = len - pos;
			if (n > count)
				n = count;
			if (n <= 0)
				return 0;

			var nInt = (int) n; // Safe because n <= count, which is an Int32
			if (nInt < 0)
				nInt = 0; // _position could be beyond EOF
			if (_buffer != null)
				unsafe {
					byte* pointer = null;
					RuntimeHelpers.PrepareConstrainedRegions();
					try {
						_buffer.AcquirePointer(ref pointer);
						Buffer.MemoryCopy(buffer, offset, pointer + pos + _offset, 0, nInt);
					} finally {
						if (pointer != null)
							_buffer.ReleasePointer();
					}
				}
			else
				unsafe {
					Buffer.MemoryCopy(buffer, offset, _mem + pos, 0, nInt);
				}

			Interlocked.Exchange(ref _position, pos + n);
			return nInt;
		}

		[SecuritySafeCritical]
		public override int ReadByte() {
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if (!CanRead)
				throw new IOException("Reading not supported.");

			long pos = Interlocked.Read(ref _position);
			long len = Interlocked.Read(ref _length);
			if (pos >= len)
				return -1;
			Interlocked.Exchange(ref _position, pos + 1);
			int result;
			if (_buffer != null)
				unsafe {
					byte* pointer = null;
					RuntimeHelpers.PrepareConstrainedRegions();
					try {
						_buffer.AcquirePointer(ref pointer);
						result = *(pointer + pos + _offset);
					} finally {
						if (pointer != null)
							_buffer.ReleasePointer();
					}
				}
			else
				unsafe {
					result = _mem[pos];
				}

			return result;
		}

		public override long Seek(long offset, SeekOrigin loc) {
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if ((ulong) offset > UnmanagedMemStreamMaxLength)
				throw new ArgumentOutOfRangeException(nameof(offset), "UnmanagedMemStreamLength");
			switch (loc) {
				case SeekOrigin.Begin:
					if (offset < 0)
						throw new IOException("SeekBeforeBegin");
					Interlocked.Exchange(ref _position, offset);
					break;
				case SeekOrigin.Current:
					long pos = Interlocked.Read(ref _position);
					if (offset + pos < 0)
						throw new IOException("SeekBeforeBegin");
					Interlocked.Exchange(ref _position, offset + pos);
					break;
				case SeekOrigin.End:
					long len = Interlocked.Read(ref _length);
					if (len + offset < 0)
						throw new IOException("SeekBeforeBegin");
					Interlocked.Exchange(ref _position, len + offset);
					break;
				default:
					throw new ArgumentException("InvalidSeekOrigin");
			}

			long finalPos = Interlocked.Read(ref _position);
			return finalPos;
		}

		[SecuritySafeCritical]
		public override void SetLength(long value) {
			if (value < 0)
				throw new ArgumentOutOfRangeException("length", "Need non-negative number.");
			if (_buffer != null)
				throw new NotSupportedException("UmsSafeBuffer");
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if (!CanWrite)
				throw new IOException("Writing not supported.");

			if (value > _capacity)
				throw new IOException("FixedCapacity");

			long pos = Interlocked.Read(ref _position);
			long len = Interlocked.Read(ref _length);
			if (value > len)
				unsafe {
					Buffer.ZeroMemory(_mem + len, value - len);
				}

			Interlocked.Exchange(ref _length, value);
			if (pos > value)
				Interlocked.Exchange(ref _position, value);
		}

		[SecuritySafeCritical]
		public override void Write(byte[] buffer, int offset, int count) {
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer), "Must be not null.");
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "Need non-negative number.");
			if (count < 0)
				throw new ArgumentOutOfRangeException(nameof(count), "Need non-negative number.");
			if (buffer.Length - offset < count)
				throw new ArgumentException("Invalid offset and length.");
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if (!CanWrite)
				throw new IOException("Writing not supported.");

			long pos = Interlocked.Read(ref _position);
			long len = Interlocked.Read(ref _length);
			long n = pos + count;
			if (n < 0)
				throw new IOException("StreamTooLong");
			if (n > _capacity)
				throw new NotSupportedException("FixedCapacity");

			if (_buffer == null) {
				if (pos > len)
					unsafe {
						Buffer.ZeroMemory(_mem + len, pos - len);
					}

				if (n > len)
					Interlocked.Exchange(ref _length, n);
			}

			if (_buffer != null) {
				long bytesLeft = _capacity - pos;
				if (bytesLeft < count)
					throw new ArgumentException("BufferTooSmall");

				unsafe {
					byte* pointer = null;
					RuntimeHelpers.PrepareConstrainedRegions();
					try {
						_buffer.AcquirePointer(ref pointer);
						Buffer.MemoryCopy(pointer + pos + _offset, 0, buffer, offset, count);
					} finally {
						if (pointer != null)
							_buffer.ReleasePointer();
					}
				}
			} else
				unsafe {
					Buffer.MemoryCopy(_mem + pos, 0, buffer, offset, count);
				}

			Interlocked.Exchange(ref _position, n);
		}

		[SecuritySafeCritical]
		public override void WriteByte(byte value) {
			if (!_isOpen)
				throw new IOException("Stream is closed.");
			if (!CanWrite)
				throw new IOException("Writing not supported.");

			long pos = Interlocked.Read(ref _position);
			long len = Interlocked.Read(ref _length);
			long n = pos + 1;
			if (pos >= len) {
				if (n < 0)
					throw new IOException("StreamTooLong");
				if (n > _capacity)
					throw new NotSupportedException("FixedCapacity");

				if (_buffer == null) {
					if (pos > len)
						unsafe {
							Buffer.ZeroMemory(_mem + len, pos - len);
						}

					Interlocked.Exchange(ref _length, n);
				}
			}

			if (_buffer != null)
				unsafe {
					byte* pointer = null;
					RuntimeHelpers.PrepareConstrainedRegions();
					try {
						_buffer.AcquirePointer(ref pointer);
						*(pointer + pos + _offset) = value;
					} finally {
						if (pointer != null)
							_buffer.ReleasePointer();
					}
				}
			else
				unsafe {
					_mem[pos] = value;
				}

			Interlocked.Exchange(ref _position, n);
		}

		#region Constructors and initializers

		[SecuritySafeCritical]
		protected UnmanagedMemoryStream() {
			unsafe {
				_mem = null;
			}

			_isOpen = false;
		}

		[SecuritySafeCritical]
		public UnmanagedMemoryStream(SafeBuffer buffer, long offset, long length) =>
			Initialize(buffer, offset, length, FileAccess.Read, false);

		[SecuritySafeCritical]
		public UnmanagedMemoryStream(SafeBuffer buffer, long offset, long length, FileAccess access) =>
			Initialize(buffer, offset, length, access, false);

		[SecurityCritical]
		internal UnmanagedMemoryStream(SafeBuffer buffer, long offset, long length, FileAccess access,
			bool skipSecurityCheck) =>
			Initialize(buffer, offset, length, access, skipSecurityCheck);

		[SecuritySafeCritical]
		protected void Initialize(SafeBuffer buffer, long offset, long length, FileAccess access) =>
			Initialize(buffer, offset, length, access, false);

		[SecurityCritical]
		internal void Initialize(SafeBuffer buffer, long offset, long length, FileAccess access,
			bool skipSecurityCheck) {
			if (buffer == null)
				throw new ArgumentNullException(nameof(buffer));
			if (offset < 0)
				throw new ArgumentOutOfRangeException(nameof(offset), "Need non-negative number.");
			if (length < 0)
				throw new ArgumentOutOfRangeException(nameof(length), "Need non-negative number.");
			if (buffer.ByteLength < (ulong) (offset + length))
				throw new ArgumentException("Invalid SafeBuffer length and offset.");
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (_isOpen)
				throw new InvalidOperationException("Operation called twice.");

			if (!skipSecurityCheck) {
				#pragma warning disable 618
				new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();
				#pragma warning restore 618
			}

			unsafe {
				byte* pointer = null;
				RuntimeHelpers.PrepareConstrainedRegions();
				try {
					buffer.AcquirePointer(ref pointer);
					if (pointer + offset + length < pointer)
						throw new ArgumentException("Unmanaged stream wrap around.");
				} finally {
					if (pointer != null)
						buffer.ReleasePointer();
				}
			}

			_offset = offset;
			_buffer = buffer;
			_length = length;
			_capacity = length;
			_access = access;
			_isOpen = true;
		}

		[SecurityCritical]
		public unsafe UnmanagedMemoryStream(byte* pointer, long length) =>
			Initialize(pointer, length, length, FileAccess.Read, false);

		[SecurityCritical]
		public unsafe UnmanagedMemoryStream(byte* pointer, long length, long capacity, FileAccess access) =>
			Initialize(pointer, length, capacity, access, false);

		[SecurityCritical]
		internal unsafe UnmanagedMemoryStream(byte* pointer, long length, long capacity, FileAccess access,
			bool skipSecurityCheck) =>
			Initialize(pointer, length, capacity, access, skipSecurityCheck);

		[SecurityCritical]
		protected unsafe void Initialize(byte* pointer, long length, long capacity, FileAccess access) =>
			Initialize(pointer, length, capacity, access, false);

		[SecurityCritical]
		internal unsafe void Initialize(byte* pointer, long length, long capacity, FileAccess access,
			bool skipSecurityCheck) {
			if (pointer == null)
				throw new ArgumentNullException(nameof(pointer));
			if (length < 0 || capacity < 0)
				throw new ArgumentOutOfRangeException(length < 0 ? "length" : "capacity", "Need non-negative number.");
			if (length > capacity)
				throw new ArgumentOutOfRangeException(nameof(length), "Length is greater than capacity.");
			if ((byte*) ((long) pointer + capacity) < pointer)
				throw new ArgumentOutOfRangeException(nameof(capacity), "Unmanaged stream wrap around.");
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
				throw new ArgumentOutOfRangeException(nameof(access));
			if (_isOpen)
				throw new InvalidOperationException("Operation called twice.");

			if (!skipSecurityCheck)
				#pragma warning disable 618
				new SecurityPermission(SecurityPermissionFlag.UnmanagedCode).Demand();
			#pragma warning restore 618

			_mem = pointer;
			_offset = 0;
			_length = length;
			_capacity = capacity;
			_access = access;
			_isOpen = true;
		}

		#endregion Constructors and initializers

	}
}

#endif