#if !NET40Plus

using System;

namespace SharedMemory.Unofficial.System.IO.MemoryMappedFiles {

	[Serializable]
	public enum MemoryMappedFileAccess {
		ReadWrite = 0,
		Read,
		Write, /* Write is valid only when creating views and not when creating MemoryMappedFiles. */
		CopyOnWrite,
		ReadExecute,
		ReadWriteExecute
	}

	[Serializable]
	[Flags]
	public enum MemoryMappedFileOptions {
		None = 0,
		DelayAllocatePages = 0x4000000
	}

}

#endif