# shared-memory

The Shared Memory class library provides an unofficial support for memory-mapped files on C#/.NET Framework 3.5.

It features:
* all basic classes in relations with memory-mapped files based on .NET 4.8 Framework file sources using low-level Win APIs
  * excepting .NET Framework 4+ memory-mapped file security class 
  * including .NET Framework 4+ marshalling (structure reading and writing)
  * including .NET Framework 4+ memory-mapped view accesors and streams
* a simple shared-memory wraping class to share data between processes (for exemple purpose or not)
* support for .NET Framework 3.5 (and later versions but it is useless and I did not tested)